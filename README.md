# Masked Fox

Privacy enhanced Firefox profile used on every machine I set up.
Inspired by [mozillarbs](https://github.com/LukeSmithxyz/mozillarbs).

## Features
* Uses search engines recommended by
  [privacytools.io](https://www.privacytools.io/providers/search-engines/)
* Uses some of the add-ons recommended by
  [privacytools.io](https://www.privacytools.io/browsers/#addons)
* Implements privacy related "about:config" tweaks suggested by
  [privacytools.io](https://www.privacytools.io/browsers/#about_config)
  including [disabled WebRTC](https://www.privacytools.io/browsers/#webrtc)
* VimVixen installed for vim-like bindings

## Installation

Copy into `~/.mozilla/firefox/`.

